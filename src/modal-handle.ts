export interface ModalHandle<T = never> {
	name: string;
	hash: string;
	isOpened: boolean;
	state: T|undefined;
	open: () => void;
	close: () => void;
}
