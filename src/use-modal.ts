import { useState, useCallback, useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

import { ModalHandle } from "./modal-handle";

export const useModal = <T = never>(name: string): ModalHandle<T> => {
	const [isOpened, setIsOpened] = useState<boolean>(false);
	const [state, setState] = useState<T>();

	const navigate = useNavigate();
	const location = useLocation();

	const hash = `#${name}`;

	const open = useCallback(() => {
		navigate({
			...location,
			hash
		});
	}, [navigate, location]);

	const close = useCallback(() => {
		if (!isOpened) {
			return;
		}

		navigate({
			...location,
			hash: '' // remove hash from url
		}, {replace: true});
	}, [isOpened, navigate, location]);

	useEffect(() => {
		if (location.hash === hash) {
			if (!isOpened) {
				setIsOpened(true);
			}

			setState({...(location.state as T)});
		} else {
			isOpened && setIsOpened(false);
		}
	}, [location, hash, isOpened])

	return {
		name,
		hash,
		isOpened,
		state,
		open,
		close
	};
}
